package ar.itba.edu.pod.hazel;

import com.hazelcast.mapreduce.Combiner;
import com.hazelcast.mapreduce.CombinerFactory;

public class Query4Combiner implements CombinerFactory<String, Integer, Integer> {


    @Override
    public Combiner<Integer, Integer> newCombiner(String depto) {
        return new Combiner<Integer, Integer> () {

            int counter;

            @Override
            public void combine(Integer i) {
                counter += i;
            }

            @Override
            public Integer finalizeChunk() {
                return counter;
            }

            public void reset() {
               counter = 0;
            }
        };
    }
}