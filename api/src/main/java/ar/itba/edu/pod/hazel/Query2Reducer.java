package ar.itba.edu.pod.hazel;

import ar.itba.edu.pod.hazel.Model.GenericWrapper;
import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by jperezcu on 11/13/16.
 */
public class Query2Reducer implements ReducerFactory<Integer, Integer, Double> {

    @Override
    public Reducer<Integer, Double> newReducer(Integer home) {
        return new Reducer<Integer, Double>() {
            private double sum;
            private Set<Integer> ids;

            @Override
            public void beginReduce() { // una sola vez en cada instancia
                sum = 0;
                ids = new HashSet<>();
            }

            @Override
            public void reduce(final Integer value) {
                sum ++;
                ids.add(value);
            }

            @Override
            public Double finalizeReduce() {
                return sum/ids.size();
            }
        };
    }
}