package ar.itba.edu.pod.hazel;

import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

public class Query5Mapper implements Mapper<Integer, String,String,Integer> {
    private static final long serialVersionUID = -5535922778765480945L;

    @Override
    public void map(Integer id, String place, Context<String, Integer> context) {
        context.emit(place,1);
    }
}
