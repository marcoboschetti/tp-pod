package ar.itba.edu.pod.hazel;

import ar.itba.edu.pod.hazel.Model.GenericWrapper;
import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

/**
 * Created by jperezcu on 11/13/16.
 */
public class Query2MapperOLD implements Mapper<Integer,GenericWrapper<Integer,Integer>,GenericWrapper<Integer,Integer>,Integer> {
    private static final long serialVersionUID = -5535922778765480945L;

    @Override
    public void map(Integer id, GenericWrapper<Integer,Integer> home, Context<GenericWrapper<Integer,Integer>, Integer> context) {

        context.emit(home, 1);
    }
}