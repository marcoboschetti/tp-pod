package ar.itba.edu.pod.hazel;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;
import com.hazelcast.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Query5Reducer2 implements ReducerFactory<Integer, String, List<String>> {

    @Override
    public Reducer<String, List<String>> newReducer(Integer popAmount) {
        return new Reducer<String, List<String>>() {

            List<String> matchingPlaces;

            @Override
            public void beginReduce() { // una sola vez en cada instancia
                matchingPlaces = new ArrayList<>();
            }

            @Override
            public void reduce(final String place) {
                matchingPlaces.add(place.trim());
            }

            @Override
            public List<String> finalizeReduce() {
                return matchingPlaces;
            }
        };
    }
}
