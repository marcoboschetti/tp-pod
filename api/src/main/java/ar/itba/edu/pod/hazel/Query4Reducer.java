package ar.itba.edu.pod.hazel;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

/**
 * Created by tritoon on 09/11/16.
 */
public class Query4Reducer implements ReducerFactory<String, Integer, Integer> {


    @Override
    public Reducer<Integer,Integer> newReducer(String s) {
        return new Reducer<Integer,Integer>() {

            int counter;

            @Override
            public void beginReduce() { // una sola vez en cada instancia
                counter = 0;
            }

            @Override
            public void reduce(Integer i) {
                counter += i;
            }

            @Override
            public Integer finalizeReduce() {
                return counter;
            }
        };
    }
}
