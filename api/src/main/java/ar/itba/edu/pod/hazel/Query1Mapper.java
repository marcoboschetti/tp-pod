package ar.itba.edu.pod.hazel;

import ar.itba.edu.pod.hazel.Model.AgeRange;
import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

/**
 * Created by tritoon on 09/11/16.
 */
public class Query1Mapper implements Mapper<Integer, Integer,AgeRange,Integer> {
    private static final long serialVersionUID = -5535922778765480945L;

    @Override
    public void map(Integer id, Integer age, Context<AgeRange, Integer> context) {
        AgeRange ageRange = null;
        if(age <= 14){
            ageRange = AgeRange.Young;
        }else if(age >= 65){
            ageRange = AgeRange.Old;
        }else{
            ageRange = AgeRange.Middle;
        }
        context.emit(ageRange,1);
    }
}
