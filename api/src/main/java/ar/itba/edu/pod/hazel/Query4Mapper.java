package ar.itba.edu.pod.hazel;

import ar.itba.edu.pod.hazel.Model.GenericWrapper;
import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

/**
 * Created by tritoon on 09/11/16.
 */
public class Query4Mapper implements Mapper<Integer, String,String, Integer> {

    private static final long serialVersionUID = -5535922778765480945L;


    @Override
    public void map(Integer id, String s, Context<String, Integer> context) {
        context.emit(s,1);
    }
}
