package ar.itba.edu.pod.hazel;

import ar.itba.edu.pod.hazel.Model.AgeRange;
import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

/**
 * Created by tritoon on 09/11/16.
 */
public class Query3Reducer implements ReducerFactory<String, int[], Double> {


    @Override
    public Reducer<int[], Double> newReducer(String s) {
        return new Reducer<int[], Double>() {

            double totalCounter, analfCounter;

            @Override
            public void beginReduce() { // una sola vez en cada instancia
                totalCounter = 0;
                analfCounter = 0;
            }

            @Override
            public void reduce(int[] ints) {
                totalCounter += ints[0];
                analfCounter += ints[1];
            }

            @Override
            public Double finalizeReduce() {
                return  analfCounter / totalCounter;
            }
        };
    }
}
