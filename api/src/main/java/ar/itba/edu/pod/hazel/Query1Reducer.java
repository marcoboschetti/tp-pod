package ar.itba.edu.pod.hazel;

import ar.itba.edu.pod.hazel.Model.AgeRange;
import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

/**
 * Created by tritoon on 09/11/16.
 */
public class Query1Reducer implements ReducerFactory<AgeRange, Integer, Integer> {

    @Override
    public Reducer<Integer, Integer> newReducer(AgeRange ageRange) {
        return new Reducer<Integer, Integer>() {
            private int sum;

            @Override
            public void beginReduce() { // una sola vez en cada instancia

                sum = 0;
            }

            @Override
            public void reduce(final Integer value) {
                sum += value;
            }

            @Override
            public Integer finalizeReduce() {
                return sum;
            }
        };
    }
}
