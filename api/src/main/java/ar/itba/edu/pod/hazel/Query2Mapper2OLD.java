package ar.itba.edu.pod.hazel;

import ar.itba.edu.pod.hazel.Model.GenericWrapper;
import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jperezcu on 11/13/16.
 */
public class Query2Mapper2OLD implements Mapper<GenericWrapper<Integer, Integer>, Integer, Integer, Integer> {
    private static final long serialVersionUID = -5535922778765480945L;
    private static Logger logger = LoggerFactory.getLogger(Query2MapperOLD.class);


    @Override
    public void map(GenericWrapper<Integer, Integer> home, Integer people, Context<Integer, Integer> context) {
        context.emit(home.getVal2(),people);
    }
}
