package ar.itba.edu.pod.hazel;

import ar.itba.edu.pod.hazel.Model.AgeRange;
import com.hazelcast.mapreduce.Combiner;
import com.hazelcast.mapreduce.CombinerFactory;

public class Query3Combiner implements CombinerFactory<String, int[], int[]> {


    @Override
    public Combiner<int[], int[]> newCombiner(String depto) {
        return new Combiner<int[], int[]>() {

            int totalCounter, analfCounter;

            @Override
            public void combine(int[] ints) {
                totalCounter+= ints[0];
                analfCounter+= ints[1];
            }

            @Override
            public int[] finalizeChunk() {
                return new int[]{totalCounter,analfCounter};
            }

            public void reset() {
                totalCounter = 0;
                analfCounter = 0;
            }
        };
    }
}