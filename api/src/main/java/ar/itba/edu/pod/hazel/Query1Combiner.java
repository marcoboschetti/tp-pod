package ar.itba.edu.pod.hazel;

import ar.itba.edu.pod.hazel.Model.AgeRange;
import com.hazelcast.mapreduce.Combiner;
import com.hazelcast.mapreduce.CombinerFactory;

public class Query1Combiner implements CombinerFactory<AgeRange, Integer, Integer> {


    @Override
    public Combiner<Integer, Integer> newCombiner(AgeRange ageRange) {

        return new Combiner<Integer, Integer>() {
            private int count;
            public void combine(Integer value) {
                count+= value;
            }
            public Integer finalizeChunk() {
                return count;
            }
            public void reset() {
                count= 0;
            }
        };

    }
}