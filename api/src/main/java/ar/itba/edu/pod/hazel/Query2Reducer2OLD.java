package ar.itba.edu.pod.hazel;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

/**
 * Created by jperezcu on 11/13/16.
 */
public class Query2Reducer2OLD implements ReducerFactory<Integer, Integer, Double> {

    @Override
    public Reducer<Integer, Double> newReducer(Integer type) {
        return new Reducer<Integer, Double>() {
            private double sum;
            private double i;

            @Override
            public void beginReduce() { // una sola vez en cada instancia
                sum = 0;
                i = 0;
            }

            @Override
            public void reduce(final Integer value) {
                sum += value;
                i++;
            }

            @Override
            public Double finalizeReduce() {
                return sum/i;
            }
        };
    }
}