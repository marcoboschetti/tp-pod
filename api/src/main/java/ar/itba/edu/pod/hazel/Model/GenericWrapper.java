package ar.itba.edu.pod.hazel.Model;

import java.io.Serializable;

/**
 * Created by tritoon on 13/11/16.
 */
public class GenericWrapper<M,N> implements Serializable{

    private M val1;
    private N val2;

    public GenericWrapper(M val1, N val2) {
        this.val1 = val1;
        this.val2 = val2;
    }

    public M getVal1() {
        return val1;
    }

    public N getVal2() {
        return val2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GenericWrapper<?, ?> that = (GenericWrapper<?, ?>) o;

        if (val1 != null ? !val1.equals(that.val1) : that.val1 != null) return false;
        return val2 != null ? val2.equals(that.val2) : that.val2 == null;

    }

    @Override
    public int hashCode() {
        int result = val1 != null ? val1.hashCode() : 0;
        result = 31 * result + (val2 != null ? val2.hashCode() : 0);
        return result;
    }
}
