package ar.itba.edu.pod.hazel;

import com.hazelcast.mapreduce.Combiner;
import com.hazelcast.mapreduce.CombinerFactory;

public class Query5Combiner implements CombinerFactory<String, Integer, Integer> {


    @Override
    public Combiner<Integer, Integer> newCombiner(String place) {

        return new Combiner<Integer, Integer>() {
            private int count;
            public void combine(Integer value) {
                count+= value;
            }
            public Integer finalizeChunk() {
                return count;
            }
            public void reset() {
                count= 0;
            }
        };

    }
}