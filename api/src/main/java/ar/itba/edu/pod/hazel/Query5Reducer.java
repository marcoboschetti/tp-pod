package ar.itba.edu.pod.hazel;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

public class Query5Reducer implements ReducerFactory<String, Integer, Integer> {

    @Override
    public Reducer<Integer, Integer> newReducer(String place) {
        return new Reducer<Integer, Integer>() {
            private int sum;

            @Override
            public void beginReduce() { // una sola vez en cada instancia

                sum = 0;
            }

            @Override
            public void reduce(final Integer value) {
                sum += value;
            }

            @Override
            public Integer finalizeReduce() {
                return sum;
            }
        };
    }
}
