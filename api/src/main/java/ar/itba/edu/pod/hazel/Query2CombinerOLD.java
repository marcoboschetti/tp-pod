package ar.itba.edu.pod.hazel;

import ar.itba.edu.pod.hazel.Model.GenericWrapper;
import com.hazelcast.mapreduce.Combiner;
import com.hazelcast.mapreduce.CombinerFactory;

/**
 * Created by jperezcu on 11/13/16.
 */
public class Query2CombinerOLD implements CombinerFactory<GenericWrapper<Integer,Integer>, Integer, Integer> {

    @Override
    public Combiner<Integer, Integer> newCombiner(GenericWrapper<Integer,Integer> home) {

        return new Combiner<Integer, Integer>() {
            private int count;
            public void combine(Integer value) {
                count+= value;
            }
            public Integer finalizeChunk() {
                return count;
            }
            public void reset() {
                count= 0;
            }
        };

    }
}