package ar.itba.edu.pod.hazel;

import ar.itba.edu.pod.hazel.Model.AgeRange;
import ar.itba.edu.pod.hazel.Model.GenericWrapper;
import com.hazelcast.logging.LoggerFactory;
import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;
import org.slf4j.Logger;

/**
 * Created by tritoon on 09/11/16.
 */
public class Query3Mapper implements Mapper<Integer, GenericWrapper<String, Boolean>,String,int[]> {

    private static final long serialVersionUID = -5535922778765480945L;


    //int[] as answer stores [total, analfabet]
    @Override
    public void map(Integer id, GenericWrapper<String, Boolean> deptoAnalf, Context<String, int[]> context) {

        context.emit(deptoAnalf.getVal1(),new int[]{1,(deptoAnalf.getVal2()?1:0)} );
    }
}
