package ar.itba.edu.pod.hazel;

import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

public class Query5Mapper2 implements Mapper<String, Integer, Integer, String> {
    private static final long serialVersionUID = -5535922778765480945L;

    @Override
    public void map(String place, Integer popNumber, Context<Integer, String> context) {
        context.emit((int)(Math.floor(popNumber/100)),place);
    }
}
