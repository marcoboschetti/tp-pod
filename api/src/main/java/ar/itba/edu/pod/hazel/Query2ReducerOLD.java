package ar.itba.edu.pod.hazel;

import ar.itba.edu.pod.hazel.Model.GenericWrapper;
import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

/**
 * Created by jperezcu on 11/13/16.
 */
public class Query2ReducerOLD implements ReducerFactory<GenericWrapper<Integer,Integer>, Integer, Integer> {

    @Override
    public Reducer<Integer, Integer> newReducer(GenericWrapper<Integer,Integer> home) {
        return new Reducer<Integer, Integer>() {
            private int sum;

            @Override
            public void beginReduce() { // una sola vez en cada instancia
                sum = 0;
            }

            @Override
            public void reduce(final Integer value) {
                sum += value;
            }

            @Override
            public Integer finalizeReduce() {
                return sum;
            }
        };
    }
}