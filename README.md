# README #
### Preparando el entorno ###
Una vez que se descarga el código fuente de la aplicación, se debe abrir una terminal y posicionado en el directorio padre del proyecto y ejecutar el archivo builder.sh, el cual se encarga de generar los ejecutables necesarios de la aplicación.
Para ejecutar cada uno de los nodos del cluster, se debe copiar el contenido del directorio server/target/tp-pod-server-1.0-SNAPSHOT/ en cada uno de los nodos del cluster. En los nodos a los que se desea conectar el cliente directamente, se debe obtener la máscara de red poner dicho valor en el archivo hazelcast.xml,  dentro del tag elemento <interface>. En cada nodo del cluster se debe ejecutar el archivo run-node.sh y esperar a que todos inicien correctamente.
Para correr el cliente, se debe tener un nodo (que puede estar afuera del cluster) con el contenido de la carpeta client/target/tp-pod-client-1.0-SNAPSHOT/ que se generó en la ejecución de builder.sh. En esta carpeta se debe ejecutar el archivo run-client.sh con una invocación similar a la siguiente:



```
#!bash

bash run-client.sh -Duser=userName -Dpass=onePass -Dquery=1 -DinPath=files/dataset-10000.csv -Daddresses=10.16.32.164 -DoutPath=answer.txt  -Dn=100 -Dtope=100 -Dprov=Catamarca 
```



Donde las variables responden a los siguientes parámetros:
* user: permite configurar el nombre de usuario del cluster. Si este parámetro no está presente en la invocación, se utilizará el nombre de usuario por defecto (que coincide con el nombre por defecto del cluster al que pertenecen los nodos del servidor dentro del hazelcast.xml)

* pass: la contraseña del usuario en el cluster al que se desea conectar el cliente. Si este parámetro no está presente en la invocación, se utilizará la contraseña por defecto (que coincide con el nombre por defecto del cluster al que pertenecen los nodos del servidor dentro del hazelcast.xml)

* query: El número de la query que se desea ejecutar. Debe ser un valor entero entre 1 y 5.
* inPath: el archivo que contiene todos los datos del censo a analizar, ubicado de forma local en el cliente.
* addresses: la IP del nodo al que se desea conectar el cliente. Este nodo tiene que tener la configuración mencionada previamente.

* outPath: el nombre del archivo con el que se exportarán los datos obtenidos. Si el archivo ya existe, será reemplazado por el nuevo archivo. De lo contrario, será creado en el directorio donde se ejecuta el cliente.
* Parámetros de las consultas:

* n: requerido para la query 3.

* prov: requerido para la query 4.

* tope: requerido para la query 4.