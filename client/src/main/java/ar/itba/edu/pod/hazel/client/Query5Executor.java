package ar.itba.edu.pod.hazel.client;

import ar.itba.edu.pod.hazel.*;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICompletableFuture;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.Collator;
import com.hazelcast.mapreduce.Job;
import com.hazelcast.mapreduce.JobTracker;
import com.hazelcast.mapreduce.KeyValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * Created by tritoon on 09/11/16.
 */
public class Query5Executor {

    private static Logger logger = LoggerFactory.getLogger(Query1Executor.class);

    public static void execute(HazelcastInstance client, String mapName, String mapName2, String dataSet, String outputFile) {

        // Preparar la particion de datos y distribuirla en el cluster a traves
        // del IMap
        final IMap<Integer, String> map = Query5Loader.loadData(client,mapName,dataSet);

        // Ahora el JobTracker y los Workers!
        final JobTracker tracker = client.getJobTracker("default");

        // Ahora el Job desde los pares(key, Value) que precisa MapReduce
        final KeyValueSource<Integer, String>  source = KeyValueSource.fromMap(map);
        final Job<Integer, String>  job = tracker.newJob(source);


        logger.info(" Inicio del primer trabajo map/reduce");
        // Obtengo la cantidad de habitantes de cada depto!!!
        // Orquestacion de Jobs y lanzamiento
        final ICompletableFuture<Map<String, Integer>> future = job
                .mapper(new Query5Mapper())
                .combiner(new Query5Combiner())
                .reducer(new Query5Reducer())
                .submit();


        // Tomar resultado e Imprimirlo
        try {
            final Map<String, Integer> peoplePerPlace = future.get();
            logger.info(" Fin del primer trabajo map/reduce");

            final IMap<String, Integer> map2 = client.getMap(mapName2);
            map2.putAll(peoplePerPlace);

            // Ahora el Job desde los pares(key, Value) que precisa MapReduce
            final KeyValueSource<String, Integer>  source2 = KeyValueSource.fromMap(map2);
            final Job<String, Integer> job2 = tracker.newJob(source2);

            logger.info(" Inicio del segundo trabajo map/reduce");
            // Orquestacion de Jobs y lanzamiento
            final ICompletableFuture<Map<Integer, List<String>>> future2 = job2
                    .mapper(new Query5Mapper2())
                    .reducer(new Query5Reducer2())
                    .submit();

            final Map<Integer, List<String>> pairedPlaces = future2.get();
            logger.info(" Fin del segundo trabajo map/reduce");

            PrintWriter writer = new PrintWriter(outputFile, "UTF-8");

            for(Map.Entry<Integer, List<String>> entry: pairedPlaces.entrySet()) {
                List<String> list = entry.getValue();
                if(list.size() > 1){
                    writer.println(String.format("%d",entry.getKey()*100));
                }
                for (int i = 0; i < list.size(); i++) {
                    for (int j = i + 1; j < list.size(); j++) {
                        writer.println(list.get(i) + " + " +list.get(j));
                    }
                }
                if(list.size() > 1) {
                    writer.println("\n");
                }
            }
            writer.close();

        } catch (InterruptedException | ExecutionException | FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        map.destroy();

    }

}
