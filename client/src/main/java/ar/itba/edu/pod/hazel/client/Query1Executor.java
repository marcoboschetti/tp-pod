package ar.itba.edu.pod.hazel.client;

import ar.itba.edu.pod.hazel.Model.AgeRange;
import ar.itba.edu.pod.hazel.Query1Combiner;
import ar.itba.edu.pod.hazel.Query1Mapper;
import ar.itba.edu.pod.hazel.Query1Reducer;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICompletableFuture;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.Job;
import com.hazelcast.mapreduce.JobTracker;
import com.hazelcast.mapreduce.KeyValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.ExecutionException;


public class Query1Executor {
    private static Logger logger = LoggerFactory.getLogger(Query1Executor.class);


    public static void execute(HazelcastInstance client, String mapName, String dataSet, String outputFile) {

        // Preparar la particion de datos y distribuirla en el cluster a traves
        // del IMap
        final IMap<Integer, Integer> map = Query1Loader.loadData(client,mapName,dataSet);

        // Ahora el JobTracker y los Workers!
        final JobTracker tracker = client.getJobTracker("default");

        // Ahora el Job desde los pares(key, Value) que precisa MapReduce
        final KeyValueSource<Integer,Integer> source = KeyValueSource.fromMap(map);
        final Job<Integer, Integer> job = tracker.newJob(source);


        logger.info(" Inicio del trabajo map/reduce");
        // Orquestacion de Jobs y lanzamiento
        final ICompletableFuture<Map<AgeRange, Integer>> future = job
                .mapper(new Query1Mapper())
                .combiner(new Query1Combiner())
                .reducer(new Query1Reducer())
                .submit();

        // Tomar resultado e Imprimirlo
        try {
            PrintWriter writer = new PrintWriter(outputFile, "UTF-8");
            final Map<AgeRange, Integer> a = future.get();
            logger.info("  Fin del trabajo map/reduce");

            writer.println(String.format("0-14 = %d",a.get(AgeRange.Young)));
            writer.println(String.format("15-64 = %d",a.get(AgeRange.Middle)));
            writer.println(String.format("65-? = %d",a.get(AgeRange.Old)));

            writer.close();
        } catch (InterruptedException | ExecutionException | FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        map.destroy();
    }
}
