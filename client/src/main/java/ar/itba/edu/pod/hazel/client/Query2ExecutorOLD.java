package ar.itba.edu.pod.hazel.client;

import ar.itba.edu.pod.hazel.*;
import ar.itba.edu.pod.hazel.Model.GenericWrapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICompletableFuture;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.Job;
import com.hazelcast.mapreduce.JobTracker;
import com.hazelcast.mapreduce.KeyValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by jperezcu on 11/13/16.
 */
public class Query2ExecutorOLD {

    private static Logger logger = LoggerFactory.getLogger(Query2ExecutorOLD.class);


    public static void execute(HazelcastInstance client, String mapName, String mapName2, String dataSet, String outputFile) {

        // Preparar la particion de datos y distribuirla en el cluster a traves
        // del IMap
        final IMap<Integer, GenericWrapper<Integer, Integer>> map = Query2Loader.loadData(client, mapName, dataSet);

        // Ahora el JobTracker y los Workers!
        final JobTracker tracker = client.getJobTracker("default");

        // Ahora el Job desde los pares(key, Value) que precisa MapReduce
        final KeyValueSource<Integer, GenericWrapper<Integer, Integer>> source = KeyValueSource.fromMap(map);
        final Job<Integer, GenericWrapper<Integer, Integer>> job = tracker.newJob(source);





        logger.info(" Inicio del primer trabajo map/reduce");
        // Orquestacion de Jobs y lanzamiento
        final ICompletableFuture<Map<GenericWrapper<Integer, Integer>, Integer>> future = job
                .mapper(new Query2MapperOLD())
                .combiner(new Query2CombinerOLD())
                .reducer(new Query2ReducerOLD())
                .submit();

        // Tomar resultado e Imprimirlo
        try {
            final Map<GenericWrapper<Integer, Integer>, Integer> peoplePerHome = future.get();
            logger.info("  Fin del primer trabajo map/reduce");

            final IMap<GenericWrapper<Integer, Integer>, Integer> map2 = client.getMap(mapName2);

            map2.putAll(peoplePerHome);


            // Ahora el Job desde los pares(key, Value) que precisa MapReduce
            final KeyValueSource<GenericWrapper<Integer, Integer>, Integer> source2 = KeyValueSource.fromMap(map2);
            final Job<GenericWrapper<Integer, Integer>, Integer> job2 = tracker.newJob(source2);

            logger.info(" Inicio del segundo trabajo map/reduce");
            // Orquestacion de Jobs y lanzamiento
            final ICompletableFuture<Map<Integer, Double>> future2 = job2
                    .mapper(new Query2Mapper2OLD())
                    .reducer(new Query2Reducer2OLD())
                    .submit();


            final Map<Integer, Double> avgPerHomeType = future2.get();
            logger.info("  Fin del segundo trabajo map/reduce");


            PrintWriter writer = new PrintWriter(outputFile, "UTF-8");

            for (Map.Entry e : avgPerHomeType.entrySet()) {
               writer.println(String.format("%d = %.2f", e.getKey(), e.getValue()));
            }
            writer.close();


        } catch (InterruptedException | ExecutionException | FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        map.destroy();
    }
}
