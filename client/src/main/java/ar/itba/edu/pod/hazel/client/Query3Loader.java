package ar.itba.edu.pod.hazel.client;

import ar.itba.edu.pod.hazel.Model.GenericWrapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

/**
 * Created by tritoon on 09/11/16.
 */
public class Query3Loader {
    private static Logger logger = LoggerFactory.getLogger(Query3Executor.class);

    public static IMap<Integer, GenericWrapper<String, Boolean>> loadData(HazelcastInstance client, String mapName, String dataset) {
        int id = 0;

        final IMap<Integer, GenericWrapper<String,Boolean>> map = client.getMap(mapName);

        final InputStream is = Client.class.getClassLoader().getResourceAsStream(dataset);
        if(is == null){
            logger.error(String.format("File %s not found",dataset));
            return null;
        }

        final LineNumberReader reader = new LineNumberReader(new InputStreamReader(is));

        String line = null;
        try {
            reader.readLine();
            while ((line = reader.readLine()) != null) {
                String[] values = line.split(",");
                map.put(id++,new GenericWrapper<>(String.format("%s!%s",values[6].trim(),values[7].trim()   ),values[4].equals("2")));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;
    }

}
