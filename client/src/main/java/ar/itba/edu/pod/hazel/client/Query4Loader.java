package ar.itba.edu.pod.hazel.client;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

/**
 * Created by tritoon on 09/11/16.
 */
public class Query4Loader {

    private static Logger logger = LoggerFactory.getLogger(Query4Executor.class);

    public static IMap<Integer, String> loadData(HazelcastInstance client, String mapName, String dataset, String prov) {
        int id = 0;

        final IMap<Integer, String> map = client.getMap(mapName);

        final InputStream is = Client.class.getClassLoader().getResourceAsStream(dataset);

        if (is == null) {
            logger.error(String.format("File %s not found", dataset));
            return null;
        }

        final LineNumberReader reader = new LineNumberReader(new InputStreamReader(is));
        String line = null;
        try {
            reader.readLine();
            while ((line = reader.readLine()) != null) {
                String[] values = line.split(",");
                if(values[7].trim().equals(prov)){
                    map.put(id++,values[6].trim());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;
    }

}
