package ar.itba.edu.pod.hazel.client;

import ar.itba.edu.pod.hazel.Model.GenericWrapper;
import ar.itba.edu.pod.hazel.*;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICompletableFuture;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.Job;
import com.hazelcast.mapreduce.JobTracker;
import com.hazelcast.mapreduce.KeyValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by jperezcu on 11/13/16.
 */
public class Query2Executor {

    private static Logger logger = LoggerFactory.getLogger(Query2Executor.class);


    public static void execute(HazelcastInstance client, String mapName, String mapName2, String dataSet, String outputFile) {

        // Preparar la particion de datos y distribuirla en el cluster a traves
        // del IMap
        final IMap<Integer, GenericWrapper<Integer, Integer>> map = Query2Loader.loadData(client, mapName, dataSet);

        // Ahora el JobTracker y los Workers!
        final JobTracker tracker = client.getJobTracker("default");

        // Ahora el Job desde los pares(key, Value) que precisa MapReduce
        final KeyValueSource<Integer, GenericWrapper<Integer, Integer>> source = KeyValueSource.fromMap(map);
        final Job<Integer, GenericWrapper<Integer, Integer>> job = tracker.newJob(source);


        logger.info(" Inicio del primer trabajo map/reduce");
        // Orquestacion de Jobs y lanzamiento
        final ICompletableFuture<Map<Integer, Double>> future = job
                .mapper(new Query2Mapper())
                .reducer(new Query2Reducer())
                .submit();

        try {
            final Map<Integer, Double> peoplePerHome = future.get();
            logger.info("  Fin del primer trabajo map/reduce");

            PrintWriter writer = new PrintWriter(outputFile, "UTF-8");

            for (Map.Entry e : peoplePerHome.entrySet()) {
                writer.println(String.format("%d = %.2f", e.getKey(), e.getValue()));
            }
            writer.close();


        } catch (InterruptedException | ExecutionException | FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        map.destroy();
    }
}
