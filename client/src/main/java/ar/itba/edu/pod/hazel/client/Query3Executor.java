package ar.itba.edu.pod.hazel.client;

import ar.itba.edu.pod.hazel.*;
import ar.itba.edu.pod.hazel.Model.AgeRange;
import ar.itba.edu.pod.hazel.Model.GenericWrapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICompletableFuture;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.Job;
import com.hazelcast.mapreduce.JobTracker;
import com.hazelcast.mapreduce.KeyValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * Created by tritoon on 09/11/16.
 */
public class Query3Executor {

    private static Logger logger = LoggerFactory.getLogger(Query3Executor.class);


    public static void execute(HazelcastInstance client, String mapName, String dataSet, int N, String outputFile) {

        // Preparar la particion de datos y distribuirla en el cluster a traves
        // del IMap
        final IMap<Integer, GenericWrapper<String, Boolean>> map = Query3Loader.loadData(client, mapName, dataSet);

        // Ahora el JobTracker y los Workers!
        final JobTracker tracker = client.getJobTracker("default");

        // Ahora el Job desde los pares(key, Value) que precisa MapReduce
        final KeyValueSource<Integer, GenericWrapper<String, Boolean>> source = KeyValueSource.fromMap(map);
        final Job<Integer, GenericWrapper<String, Boolean>> job = tracker.newJob(source);

        logger.info(" Inicio del trabajo map/reduce");
        // Orquestacion de Jobs y lanzamiento
        final ICompletableFuture<Map<String, Double>> future = job
                .mapper(new Query3Mapper())
                .combiner(new Query3Combiner())
                .reducer(new Query3Reducer())
                .submit();

        // Tomar resultado e Imprimirlo
        try {
            final Map<String, Double> a = future.get();
            logger.info(" Fin del trabajo map/reduce");

            Map<String, Double> sortedMap = sortByValue(a);
            Iterator<Map.Entry<String, Double>> it = sortedMap.entrySet().iterator();


            PrintWriter writer = new PrintWriter(outputFile, "UTF-8");

            int count = 0;

            while (count < N && it.hasNext()) {
                Map.Entry<String, Double> entry = it.next();
                writer.println(String.format("%s = %.2f", entry.getKey().split("!")[0], entry.getValue()));
                count++;
            }

            writer.close();
        } catch (InterruptedException | ExecutionException | FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        map.destroy();

    }


    public static Map<String, Double> sortByValue(Map<String, Double> map) {
        List<Map.Entry<String, Double>> list = new LinkedList<>(map.entrySet());

        Collections.sort(list, (o1, o2) -> (o2.getValue()).compareTo(o1.getValue()));

        Map<String, Double> result = new LinkedHashMap<>();
        for (Map.Entry<String, Double> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
}
