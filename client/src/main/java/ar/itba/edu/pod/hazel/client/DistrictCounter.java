package ar.itba.edu.pod.hazel.client;

/**
 * Created by tritoon on 23/11/16.
 */
public class DistrictCounter implements Comparable<DistrictCounter>{

    private final String district;
    private final int people;

    public DistrictCounter(String district, int people) {
        this.district = district;
        this.people = people;
    }

    public String getDistrict() {
        return district;
    }

    public int getPeople() {
        return people;
    }


    @Override
    public int compareTo(DistrictCounter o) {
        return o.getPeople() - this.people;
    }
}
