package ar.itba.edu.pod.hazel.client;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.core.HazelcastInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by tritoon on 09/11/16.
 */
public class ClientInitializer {

    private static Logger logger = LoggerFactory.getLogger(Query1Executor.class);

    private static final String DEV_NAME = "55266-49729-testing";
    private static final String PASS = "XXX55266-49729-testing";


    public static HazelcastInstance initialize(QueryParameters parameters) {

        //Valido query
        String queryS =  System.getProperty("query");
        if (queryS == null) {
            logger.info("Query parameter is required");
            return null;
        }
        int query = -1;


        try {
            query = Integer.valueOf(queryS);
        }catch (NumberFormatException e){
            logger.info("Query parameter should be an integer");
            return null;
        }

        if(query<1 || query > 5){
            logger.info("Query parameter needs to be between 1 and 5");
            return null;
        }
        parameters.setQuery(query);
        // Termino de validar query

        if(query == 3){
            String nS =  System.getProperty("n");

            if (nS == null) {
                logger.info("n parameter is required");
                return null;
            }
            int n;

            try {
                n = Integer.valueOf(nS);
            }catch (NumberFormatException e){
                logger.info("n parameter should be an integer");
                return null;
            }
            parameters.setN(n);
        }

        if(query == 4){
            String topeS =  System.getProperty("tope");

            if (topeS == null) {
                logger.info("tope parameter is required");
                return null;
            }
            int tope;

            try {
                tope = Integer.valueOf(topeS);
            }catch (NumberFormatException e){
                logger.info("tope parameter should be an integer");
                return null;
            }
            parameters.setTope(tope);

            String provS =  System.getProperty("prov");
            if (provS == null) {
                logger.info("prov parameter is required");
                return null;
            }
            parameters.setProv(provS);
        }



        // Parseo inPath
        String inputS =  System.getProperty("inPath");
        if (inputS == null) {
            logger.info("inPath parameter required");
            return null;
        }
        parameters.setInputFile(inputS);
        // Termino de parsear inPath

        // Parseo outPath
        String outputS =  System.getProperty("outPath");
        if (outputS == null) {
            logger.info("outPath parameter required");
            return null;
        }
        parameters.setOutputFile(outputS);
        // Termino de parsear outPath


        // Parseo user
        String userS =  System.getProperty("user");
        if (userS == null) {
            logger.info(String.format("user parameter not found, taking %s as default",DEV_NAME));
            userS = DEV_NAME;
        }
        // Termino de parsear user

        // Parseo pass
        String passS =  System.getProperty("pass");
        if (passS == null) {
            logger.info(String.format("pass parameter not found, taking %s as default",PASS));
            passS = PASS;
        }
        // Termino de parsear pass

        final ClientConfig ccfg = new ClientConfig();
        ccfg.getGroupConfig().setName(userS).setPassword(passS);

        // no hay descubrimiento automatico,
        // pero si no decimos nada intentará usar LOCALHOST
       final String addresses = System.getProperty("addresses");

        if (addresses != null) {
            final String[] arrayAddresses = addresses.split("[,;]");
            final ClientNetworkConfig net = new ClientNetworkConfig();
            net.addAddress(arrayAddresses);
            ccfg.setNetworkConfig(net);
        }else{
            logger.info("No addresses given, localhost selected by default");
        }

        return HazelcastClient.newHazelcastClient(ccfg);
    }

}
