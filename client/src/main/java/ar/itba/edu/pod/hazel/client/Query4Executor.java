package ar.itba.edu.pod.hazel.client;

import ar.itba.edu.pod.hazel.*;
import ar.itba.edu.pod.hazel.Model.GenericWrapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICompletableFuture;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.Job;
import com.hazelcast.mapreduce.JobTracker;
import com.hazelcast.mapreduce.KeyValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * Created by tritoon on 09/11/16.
 */
public class Query4Executor {
    private static Logger logger = LoggerFactory.getLogger(Query4Executor.class);


    public static void execute(HazelcastInstance client, String mapName, String dataSet, int tope, String prov,
                               String outputFile) {

        // Preparar la particion de datos y distribuirla en el cluster a traves
        // del IMap
        final IMap<Integer, String> map = Query4Loader.loadData(client,mapName,dataSet,prov);

        // Ahora el JobTracker y los Workers!
        final JobTracker tracker = client.getJobTracker("default");

        // Ahora el Job desde los pares(key, Value) que precisa MapReduce
        final KeyValueSource<Integer, String>  source = KeyValueSource.fromMap(map);
        final Job<Integer, String>  job = tracker.newJob(source);

        logger.info(" Inicio del trabajo map/reduce");
        // Orquestacion de Jobs y lanzamiento
        final ICompletableFuture<Map<String, Integer>> future = job
                .mapper(new Query4Mapper())
                .combiner(new Query4Combiner())
                .reducer(new Query4Reducer())
                .submit();

        // Tomar resultado e Imprimirlo
        try {
            final Map<String, Integer> a = future.get();
            logger.info(" Fin del trabajo map/reduce");

            List<DistrictCounter> districts = new LinkedList<>();
            for(Map.Entry<String, Integer> entry:a.entrySet()){
                if(entry.getValue() < tope){
                    districts.add(new DistrictCounter(entry.getKey(),entry.getValue()));
                }
            }

            Collections.sort(districts);
            PrintWriter writer = new PrintWriter(outputFile, "UTF-8");

            for(DistrictCounter d:districts) {
                writer.println(String.format("%s = %d", d.getDistrict(), d.getPeople()));
            }

            writer.close();
        } catch (InterruptedException | ExecutionException | FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        map.destroy();
    }

}
