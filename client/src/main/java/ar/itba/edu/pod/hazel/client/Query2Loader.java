package ar.itba.edu.pod.hazel.client;

import ar.itba.edu.pod.hazel.Model.GenericWrapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

/**
 * Created by jperezcu on 11/13/16.
 */
public class Query2Loader {
    private static Logger logger = LoggerFactory.getLogger(Query2ExecutorOLD.class);

    public static IMap<Integer, GenericWrapper<Integer, Integer>> loadData(HazelcastInstance client, String mapName, String dataset) {
        int id = 0;

        final IMap<Integer, GenericWrapper<Integer, Integer>> map = client.getMap(mapName);

        final InputStream is = Client.class.getClassLoader().getResourceAsStream(dataset);

        if (is == null) {
            logger.error(String.format("File %s not found", dataset));
            return null;
        }

        final LineNumberReader reader = new LineNumberReader(new InputStreamReader(is));
        String line = null;
        try {
            reader.readLine();
            while ((line = reader.readLine()) != null) {
                String[] values = line.split(",");
//                final Home home = new Home( Integer.valueOf(values[8]), Integer.valueOf(values[0]) );
                //      int[] home = new int[]{ Integer.valueOf(values[8]), Integer.valueOf(values[0])};
                map.put(id++, new GenericWrapper<>(Integer.valueOf(values[8]), Integer.valueOf(values[0])));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;
    }
}
