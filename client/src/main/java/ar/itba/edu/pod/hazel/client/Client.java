package ar.itba.edu.pod.hazel.client;

import com.hazelcast.core.HazelcastInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

//DATASET = "files/dataset-10000.csv";

public class Client {
    private static Logger logger = LoggerFactory.getLogger(Client.class);

    private static final String MAP_NAME = "55266-49729-map";
    private static final String MAP_NAME2 ="55266-49729-map2" ;

    public static void main(final String[] args)
            throws InterruptedException, ExecutionException, IOException, URISyntaxException {

        logger.info(" Inicio de la lectura del archivo");
        QueryParameters parameters = new QueryParameters();
        final HazelcastInstance client = ClientInitializer.initialize(parameters);

        logger.info(" Fin de lectura del archivo");

        if(client == null){
            return;
        }

//Changes file mapname

        String mapName = String.format("%s%d",MAP_NAME,System.currentTimeMillis());
        String mapName2 = String.format("%s%d",MAP_NAME2,System.currentTimeMillis());

        switch (parameters.getQuery()){
            case 1:
                Query1Executor.execute(client,mapName,parameters.getInputFile(),parameters.getOutputFile());
                break;
            case 2:
                Query2Executor.execute(client,mapName,mapName2,parameters.getInputFile(),parameters.getOutputFile());
                break;
            case 3:
                Query3Executor.execute(client,mapName,parameters.getInputFile(),parameters.getN(),
                        parameters.getOutputFile());
                break;
            case 4:
                Query4Executor.execute(client,mapName,parameters.getInputFile(),
                        parameters.getTope(),parameters.getProv(),parameters.getOutputFile());
                break;
            case 5:
                Query5Executor.execute(client,mapName,mapName2,parameters.getInputFile(), parameters.getOutputFile());
                break;
        }

        System.exit(0);
    }
}
