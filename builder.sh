#!/bin/bash

mvn clean install
cd server/target
tar -vzxf tp-pod-server-1.0-SNAPSHOT-bin.tar.gz
cd ../..

cd client/target
tar -vzxf tp-pod-client-1.0-SNAPSHOT-bin.tar.gz
cd ../..

echo Done, have fun =D
